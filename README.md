# import-policy

## What is this?

The pipeline will handle listed actions for SCPs: import existing policy and modify policy.

Terraform command "terraform import aws_organizations_policy.policy-demo-import policy-id" will import policy for policy-id mentioned in the command.

Terraform resource “aws_organizations_policy” be used for modification of policy imported.

The jobs to be run using docker executor from the docker image with all necessary services that are needed to execute our pipeline
Test pipeline to be invoked manually after pull request files is approved.
Prod pipeline to be invoked manually after merging the feature branch to master.
The state information to be stored in Shared Services account
 Provision a bucket in shared services to save Outputstate.

1. Configuration file to be configured to execute the following steps
2. Import/Modify SCP – terraform script
3. Execute test scripts
4. Save test results to Designated folder
6. Manual verification of test results and merge to master

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written. Reading [Terraform: Up and Running](https://www.amazon.com/Terraform-Running-Writing-Infrastructure-Code-ebook/dp/B06XKHGJHP) is also recommended.

## How the repository is structured

Each environment has its own directory as follows -
1. Terraform scripts are saved in tfscripts folder.
 - main.tf
 - backend.tf
 - variable.tf
2. SCP policies are saved under aws-scp policy.
 - policy-import-demo.json  

## How to run the CI/CD pipeline

The CI/CD pipeline will start running once we commit the files to the project repository.

