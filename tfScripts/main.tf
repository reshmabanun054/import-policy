resource "aws_organizations_policy" "policy-demo-import" {
  name ="policy-demo-import"
  content = jsonencode(jsondecode(file(var.file_path)))
}
