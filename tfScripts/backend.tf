terraform {  
    backend "s3" {
        bucket  = "tf-st-files"
        key     = "import-policy/Outputstate/terraform.tfstate"
        region  = "us-east-1"
    }
}
